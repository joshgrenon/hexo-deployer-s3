var level = require('level')
  , s3sync = require('s3-sync')
  , readdirp = require('readdirp')



var public_dir = hexo.config.public_dir || './public';


hexo.extend.deployer.register('s3', function (args, callback) {
  var config = hexo.config.deploy;

  if (!config.bucket || !config.aws_key || !config.aws_secret){
    var help = [
      'You should configure deployment settings in _config.yml first!',
      '',
      'Example:',
      '  deploy:',
      '    type: s3',
      '    bucket: <bucket>',
      '    aws_key: <aws_key>',
      '    aws_secret: <aws_secret>',
      '    [concurrency]: <concurrency>',
      '',
      'For more help, you can check the docs: ' + 'https://github.com/joshstrange/hexo-deployer-s3'
    ];

    console.log(help.join('\n'));
    return callback();
  }


  var files = readdirp({
      root: public_dir,
      entryType: 'both'
  });

  if(!config.concurrency)
  {
    config.concurrency = 8;
  }

  // Takes the same options arguments as `knox`,
  // plus some additional options listed above
  var uploader = s3sync({
      key: config.aws_key
    , secret: config.aws_secret
    , bucket: config.bucket
    , concurrency: config.concurrency
  }).on('data', function(file) {
    console.log(file.fullPath + ' -> ' + file.url)
  }).on('end', function() {
    console.log('Done!');
    callback();
  });

  files.pipe(uploader);
  

});
